package ar.com.bitsense.checker.monkey.domain;

/**
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
public class SMSStatus {

    public static String OK = "OK";
    public static String FAIL = "FAIL";
    public static String RETRY = "RETRY";
    public static String NEW = "NEW";
}
