package ar.com.bitsense.checker.monkey.domain;

/**
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
public class SMSDirection {

    public static String IN = "IN";

    public static String OUT = "OUT";
}
