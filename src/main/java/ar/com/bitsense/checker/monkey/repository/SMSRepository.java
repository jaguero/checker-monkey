package ar.com.bitsense.checker.monkey.repository;

import ar.com.bitsense.checker.monkey.domain.SMS;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
public interface SMSRepository extends JpaRepository<SMS, Long> {

    @Query("SELECT s FROM SMS s WHERE s.direction = 'IN' AND s.status = 'FAIL' ")
    public List<SMS> findInRetry();

}
