package ar.com.bitsense.checker.monkey.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
@Entity
@Table(name = "sms")
public class SMS implements Cloneable {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "recipient", nullable = true)
    private Long recipient;

    @Column(name = "slot", nullable = true)
    private String slot;

    @Column(name = "slotgroup", nullable = true)
    private String slotgroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sms_transaction_id")
    private SMSTransaction transaction;

    @Column(name = "direction", nullable = true)
    private String direction;

    @Column(name = "msg")
    private String msg;

    @Column(name = "status", nullable = true)
    private String status;

    @Column(name = "retry_count", nullable = true)
    private int retryCount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "datetime", nullable = false)
    private Date dateTime;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the recipient
     */
    public Long getRecipient() {
        return recipient;
    }

    @PrePersist
    protected void onCreate() {
        this.setDateTime(new Date());
    }

    /**
     * @param recipient the recipient to set
     */
    public void setRecipient(Long recipient) {
        this.recipient = recipient;
    }

    /**
     * @return the transaction
     */
    public SMSTransaction getTransaction() {
        return transaction;
    }

    /**
     * @param transaction the transaction to set
     */
    public void setTransaction(SMSTransaction transaction) {
        this.transaction = transaction;
    }

    /**
     * @return the direction
     */
    public String getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the retryCount
     */
    public int getRetryCount() {
        return retryCount;
    }

    /**
     * @param retryCount the retryCount to set
     */
    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    /**
     * @return the dateTime
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public SMS clone() throws CloneNotSupportedException {
        return (SMS) super.clone();
    }

    /**
     * @return the slot
     */
    public String getSlot() {
        return slot;
    }

    /**
     * @param slot the slot to set
     */
    public void setSlot(String slot) {
        this.slot = slot;
    }

    /**
     * @return the slotgroup
     */
    public String getSlotgroup() {
        return slotgroup;
    }

    /**
     * @param slotgroup the slotgroup to set
     */
    public void setSlotgroup(String slotgroup) {
        this.slotgroup = slotgroup;
    }

}
