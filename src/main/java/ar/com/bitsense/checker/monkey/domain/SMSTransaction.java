package ar.com.bitsense.checker.monkey.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
@Entity
@Table(name = "sms_transaction")
public class SMSTransaction {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "ip_origin")
    private String ipOrigin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "datetime", nullable = false)
    private Date dateTime;

    @OneToMany(mappedBy = "transaction", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private Collection<SMS> smsList;

    /**
     *
     */
    public SMSTransaction() {
        this.smsList = new ArrayList();
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the ipOrigin
     */
    public String getIpOrigin() {
        return ipOrigin;
    }

    @PrePersist
    protected void onCreate() {
        this.setDateTime(new Date());
    }

    /**
     * @param ipOrigin the ipOrigin to set
     */
    public void setIpOrigin(String ipOrigin) {
        this.ipOrigin = ipOrigin;
    }

    /**
     * @return the dateTime
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public void addSMS(SMS sms) {
        if (!smsList.contains(sms)) {
            smsList.add(sms);

        }
    }

    /**
     * @return the smsList
     */
    public Collection<SMS> getSmsList() {
        return smsList;
    }

    /**
     * @param smsList the smsList to set
     */
    public void setSmsList(Collection<SMS> smsList) {
        this.smsList = smsList;
    }

}
