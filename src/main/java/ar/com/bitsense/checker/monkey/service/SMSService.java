package ar.com.bitsense.checker.monkey.service;

import ar.com.bitsense.checker.monkey.domain.SMS;
import ar.com.bitsense.checker.monkey.repository.SMSRepository;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
@Service
public class SMSService {

    @Autowired
    private SMSRepository smsRepository;

    public Collection<SMS> findInRetryStatus() {
        return this.smsRepository.findInRetry();
    }

}
