package ar.com.bitsense.checker.monkey.service;

import ar.com.bitsense.checker.monkey.tasklet.CheckerTasklet;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author Juan Manuel Agüero <jaguero@flowcode.com.ar>
 */
@Service
public class NotificationService {

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    public void notifyNow() {

        final String username = "flowcode.test@gmail.com";
        final String password = "Maipu671";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        logger.info("Email notification...");
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("checker_notification@bitsense.com.ar"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("juanma.aguero@gmail.com"));
            message.setSubject("[Checker Monkey] Notification");
            message.setText("Too many Retrys, check the logs!");

            Transport.send(message);

            logger.info("Email sent");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }

}
