package ar.com.bitsense.checker.monkey.tasklet;

import ar.com.bitsense.checker.monkey.domain.SMS;
import ar.com.bitsense.checker.monkey.service.NotificationService;
import ar.com.bitsense.checker.monkey.service.SMSService;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author juanma
 */
public class CheckerTasklet implements Tasklet {
    
    private static final Logger logger = LoggerFactory.getLogger(CheckerTasklet.class);

    @Autowired
    private SMSService smsSrv;
    @Autowired
    private NotificationService notificationSrv;

    @Override
    public RepeatStatus execute(StepContribution sc, ChunkContext cc) throws Exception {
        logger.info("Checking...");
        Collection<SMS> smsList = smsSrv.findInRetryStatus();
        if (smsList.size() > 3 ) {
            notificationSrv.notifyNow();
        }

        return RepeatStatus.FINISHED;
    }

}
