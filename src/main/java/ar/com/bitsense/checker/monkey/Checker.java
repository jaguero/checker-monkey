package ar.com.bitsense.checker.monkey;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author juanma
 */
public class Checker {

    private static final Logger logger = LoggerFactory.getLogger(Checker.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String[] springConfig = {"spring/context.xml"};
        ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
        Job job = (Job) context.getBean("checkNotify");

        /* send queued sms's */
        JobParametersBuilder builder = new JobParametersBuilder();
        builder.addDate("date", new Date());
        JobExecution execution;
        logger.info("Running...");
        try {

            execution = jobLauncher.run(job, builder.toJobParameters());

            logger.info("Exit Status : " + execution.getStatus());

        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException ex) {
            System.out.println("Failed daemon. Complete error:" + ex.getMessage());
        }

    }

}
